package com.example.latihan2_intent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    Button cuacaku;
    Button scoring;
    @Override
    protected void onCreate(Bundle savedInstancestate) {

        //TOdo auto generate method stub
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_register);

        cuacaku = (Button) findViewById(R.id.btn_cuaca);
        cuacaku.setOnClickListener(this);
        scoring = (Button) findViewById(R.id.btn_score);
        scoring.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cuaca:
                Intent cuaca = new Intent(RegisterActivity.this, CuacaActivity.class);
                startActivity(cuaca);
                break;
            case R.id.btn_score:
                Intent scoreku = new Intent(RegisterActivity.this,ScoringActivity.class);
                startActivity(scoreku);
                break;
                default:
                    break;

        }
    }
}
