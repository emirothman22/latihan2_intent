package com.example.latihan2_intent;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class IntentActivity extends AppCompatActivity implements View.OnClickListener {
    Button registered;

    @Override
    protected void onCreate(Bundle savedInstancestate) {

        //TOdo auto generate method stub
        super.onCreate(savedInstancestate);
        setContentView(R.layout.activity_home);

        registered = (Button) findViewById(R.id.register);
        registered.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:
                Intent registered = new Intent(IntentActivity.this, RegisterActivity.class);
                startActivity(registered);
                break;

        }
    }
}